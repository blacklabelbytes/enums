<?php

namespace BlackLabelBytes\Enums;

class EnumerationGroups
{
    protected $newEnumerations = [];

    /**
     * @var EnumerationConfig
     */
    protected $config;

    public function __construct(EnumerationConfig $config)
    {
        $this->config = $config;
        foreach ($config->getGroups() as $group => $sources) {
            $this->newEnumerations[$group] = [];
        }
    }

    public function getGroups()
    {
        return array_keys($this->newEnumerations);
    }

    public function findEnumsInGroup(string $group)
    {
        foreach ($this->config->getEnumsInGroup($group) as $item) {
            $this->rememberIfNotExists($group, $item);
        }
    }

    public function getDescriptionsForGroup(string $group)
    {
        return collect($this->newEnumerations[$group])->flatten()->map(function ($item) {
            return $item->describe();
        })->toArray();
    }

    public function foundCountInGroup(string $group)
    {
        return sizeof($this->newEnumerations[$group]);
    }

    public function foundNewInGroup(string $group)
    {
        $count = $this->foundCountInGroup($group);
        return $count > 0;
    }

    public function rememberIfNotExists(string $group, EnumerableEntity $item)
    {
        if (!$item->exists()) {
            $this->newEnumerations[$group][] = $item;
        }
    }

    public function storeNonExistingGroup(string $group)
    {
        foreach ($this->newEnumerations[$group] as $item) {
            $item->insert();
        }
    }
}
