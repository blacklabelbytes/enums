<?php

namespace BlackLabelBytes\Enums;

interface EnumerableEntity
{
    /**
     * Get the key by which to determine uniqueness
     *
     * @return string
     */
    public function getEnumerationKey();

    /**
     * Does the enum exist
     *
     * @return bool
     */
    public function exists();

    /**
     * Insert the enumeration
     *
     * @return void
     */
    public function insert();

    /**
     * Describe the enumeration
     *
     * @return string
     */
    public function describe();
}
