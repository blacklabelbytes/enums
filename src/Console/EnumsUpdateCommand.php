<?php

namespace BlackLabelBytes\Enums\Console;

use BlackLabelBytes\Enums\EnumerationGroups;
use BlackLabelBytes\Enums\EnumerationConfig;
use Illuminate\Console\Command;
use Illuminate\Console\ConfirmableTrait;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputOption;

class EnumsUpdateCommand extends Command
{
    use ConfirmableTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'enums:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update enumeration rows in database';


    /**
     * Create a new controller creator command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(EnumerationConfig $config)
    {
        $verifier = new EnumerationGroups($config);
        $groups = $verifier->getGroups();
        $this->info("Groups: ".implode(", ", $groups));

        /**
         * Walk all groups in sequence
         */
        foreach ($groups as $group) {
            $this->info("---------------");
            $this->comment("Checking enums in group $group");
            $verifier->findEnumsInGroup($group);
            if (!$verifier->foundNewInGroup($group)) {
                $this->comment("No new enumerations found in group $group!");
                continue;
            } else {
                $descriptions = $verifier->getDescriptionsForGroup($group);
                $this->comment("---------------");
                foreach ($descriptions as $index => $description) {
                    $count = $index+1;
                    $this->getOutput()->writeln("<info>{$count}: </info>$description");
                    $this->comment("---------------");
                }
            }
            if (! $this->confirmToProceed("New or updated enumerations found! We will now proceed to seed the missing ones in group $group.", function () {
                return true;
            })) {
                $this->error("Aborted by user.");
                return;
            }
            DB::transaction(function () use ($verifier, $group) {
                $verifier->storeNonExistingGroup($group);
            });
        }
        $this->info("Complete!");
    }

    
    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['force', null, InputOption::VALUE_NONE, 'Force the operation to run without preview.'],
        ];
    }

    /**
     * Copied to avoid inheritance issues
     *
     */
    public function option($key = null)
    {
        if (is_null($key)) {
            return $this->input->getOptions();
        }
        return $this->input->getOption($key);
    }
}
