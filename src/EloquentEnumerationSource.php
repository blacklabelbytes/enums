<?php

namespace BlackLabelBytes\Enums;

use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class EloquentEnumerationSource extends EnumerationSource
{
    /**
     * Implement to return a collection of models to insert.
     *
     * @return Model[]|Collection
     */
    abstract public function getModels();

    /**
     * Return the verifiable entries
     */
    public function getEnums()
    {
        $items = collect();
        foreach ($this->getModels() as $model) {
            $entity = new EloquentEnumerableEntity($model);
            $items = $items->push($entity);
        }
        return $items;
    }
}
