<?php

namespace BlackLabelBytes\Enums;

class EnumerationConfig
{
    /**
     * A key-value array of enum group and enumeration sources
     *
     * @var array
     */
    protected $groups = [];

    public function __construct(array $config = [])
    {
        $this->groups = $config["groups"];
    }

    public function getGroups()
    {
        return $this->groups;
    }

    public function getEnumsInGroup(string $group)
    {
        $items = collect();
        foreach ($this->groups[$group] as $source) {
            $items = $items->concat((new $source)->getEnums());
        }
        return $items->toArray();
    }
}
