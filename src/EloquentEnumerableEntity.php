<?php

namespace BlackLabelBytes\Enums;

use Illuminate\Database\Eloquent\Model;

class EloquentEnumerableEntity implements EnumerableEntity
{
    /** @var Model */
    protected $eloquent;

    public function __construct(Model $eloquent)
    {
        $this->eloquent = $eloquent;
    }

    /**
     * Get the key by which to determine uniqueness
     *
     * @return string
     */
    public function getEnumerationKey()
    {
        $modelEnumKey = $this->eloquent->enumKey;
        if ($modelEnumKey !== null) {
            return $modelEnumKey;
        }
        return "key";
    }

    protected function getEnumerationKeyValue()
    {
        return $this->eloquent->getAttribute($this->getEnumerationKey());
    }

    /*
    *  Check if it exists
    */
    public function exists()
    {
        return $this->eloquent->query()
            ->where($this->getEnumerationKey(), $this->getEnumerationKeyValue())
            ->exists();
    }

    /*
    * Insert enumeration
    */
    public function insert()
    {
        $this->eloquent->save();
    }

    /*
    *   Describe the enumeration
    */
    public function describe()
    {
        return get_class($this->eloquent)." [".$this->getEnumerationKeyValue()."]";
    }
}
