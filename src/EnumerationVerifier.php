<?php

namespace BlackLabelBytes\Enums;

class EnumerationVerifier
{
    protected $newEnumerations;

    /**
     * @var EnumerationSource
     */
    protected $source;

    public function __construct(EnumerationSource $source)
    {
        $this->source = $source;
        $this->newEnumerations = collect();
    }

    public function findEnums()
    {
        foreach ($this->source->getEnums() as $item) {
            $this->rememberIfNotExists($item);
        }
    }

    public function getDescriptionOfNonexisting()
    {
        return $this->newEnumerations->map(function ($item) {
            return $item->describe();
        })->toArray();
    }

    public function foundCount()
    {
        return $this->newEnumerations->count();
    }

    public function foundNew()
    {
        return !$this->newEnumerations->isEmpty();
    }

    public function rememberIfNotExists(EnumerableEntity $item)
    {
        if (!$item->exists()) {
            $this->newEnumerations->push($item);
        }
    }

    public function storeNonExisting()
    {
        foreach ($this->newEnumerations as $item) {
            $item->insert();
        }
    }
}
