<?php

namespace BlackLabelBytes\Enums;

use BlackLabelBytes\Enums\Console\EnumsUpdateCommand;

class EnumerationServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * The config prefix for this package.
     *
     * @var string $configName
     * */
    protected $configName = 'enums';

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfig();

        $this->app->singleton(EnumerationConfig::class, function ($app) {
            return new EnumerationConfig($app['config']->get($this->configName));
        });

        $this->commands([
            EnumsUpdateCommand::class
        ]);
    }

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishConfig();
    }

    /**
     * Merge config file.
     *
     * @param void
     * @return  void
     */
    protected function mergeConfig()
    {
        $configPath = __DIR__ . '/../config/' . $this->configName . '.php';

        $this->mergeConfigFrom($configPath, $this->configName);
    }

    /**
     * Publish config file.
     *
     * @param void
     * @return  void
     */
    protected function publishConfig()
    {
        $configPath = __DIR__ . '/../config/' . $this->configName . '.php';

        $this->publishes([$configPath => config_path($this->configName . '.php')], 'impersonate');
    }
}
