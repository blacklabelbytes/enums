<?php

namespace BlackLabelBytes\Enums;

abstract class EnumerationSource
{
    /**
     * Return the enumerations
     */
    public function getEnums()
    {
        return collect();
    }
}
