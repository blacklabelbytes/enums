<?php

return [
    /**
     * Groups are a key-value array
     * - key is a group identifier
     * - value is an array of enumeration sources
     *
     * Groups will be processed in order.
     */
    'groups' => [
        /**
         * Example:
         * 'group1' => [
         *      EnumerationSourceFoo::class
         * ],
         * 'group2' => [
         *      DependentSourceBar::class
         * ],
         */
        'default' => [
            //Put enumeration sources here...
        ]
    ]
];
