FROM php:8.0-cli
LABEL maintainer="jonas@blb.fi"

RUN apt-get update && apt-get install -y \
    git \
    curl \
    sudo \
    openssh-client \
    default-mysql-client \
    netcat \
    libzip-dev \
    libicu-dev \
    libpng-dev \
    libxml2-dev
RUN docker-php-ext-install bcmath intl pdo pdo_mysql zip gd

# COMPOSER
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

WORKDIR /var/www/html
