# Enumeration insertion package

Define enumerations (types/states/etc.) in code, and insert missing ones with a single command.

- Uses a multi-level configuration for grouping enumerations together and process in order

## Installation

Register the package repository:

```
"repositories":[
        {
        "type": "git",
        "url": "https://bitbucket.org/blacklabelbytes/enums.git"
    }
]
```

Require the package:

```
"require":[
    "blacklabelbytes/enums": "^1.0"
]
```

Then Publish the enums config
```
php artisan vendor:publish --provider="BlackLabelBytes\Enums\EnumerationServiceProvider"
```


## Usage (eloquent)

- Identify a model which can generate its rows using an enumeration key (preferably a unique non-primary-key that is a machine-readable string)
- Create a new class that extends from ```BlackLabelBytes\Enums\EloquentEnumerationSource``` to generate the rows
- Register the class in the enums.php configuration file
- Run the ```php artisan enums:update``` command

## Usage (other)

- Identify an enumerable entity
- Implement your version of ```BlackLabelBytes\Enums\EnumerableEntity``` (either as a wrapper for another class or directly onto an entity)
- Create a new class that extends from ```BlackLabelBytes\Enums\EnumerationSource``` to generate the rows of your implementation of enumerable entities