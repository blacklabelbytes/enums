<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;

class TestModel extends Model
{
    public const FOO = "one";
    public const BAR = "two";

    public $enumKey = "key";
}
