<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;
use Tests\TestModel;
use Tests\TestSeeder;

class EnumerationsTest extends TestCase
{
    public function testEnumerationCommandWorks()
    {
        $this->seed(TestSeeder::class);
        $this->assertDatabaseCount('test_models', 0);

        // Run the artisan command with force
        Artisan::call('enums:update', ['--force' => true]);

        $this->assertDatabaseCount('test_models', 2);
        $this->assertDatabaseHas('test_models', [
            'key' => TestModel::FOO,
        ]);
        $this->assertDatabaseHas('test_models', [
            'key' => TestModel::BAR,
        ]);
    }
}
