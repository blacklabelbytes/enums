<?php

namespace Tests;

use BlackLabelBytes\Enums\EloquentEnumerationSource;

class TestEnumerations extends EloquentEnumerationSource
{
    public function getModels()
    {
        return collect([
            TestModel::make()->forceFill(['key' => TestModel::FOO]),
            TestModel::make()->forceFill(['key' => TestModel::BAR]),
        ]);
    }
}
