<?php

namespace Tests;

use BlackLabelBytes\Enums\EnumerationServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;

class TestCase extends Orchestra
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->setUpDatabase($this->app);
        $this->app["config"]->set('enums', [
            'groups' => [
                'group1' => [
                    TestEnumerations::class
                ]
            ]
        ]);
    }

    protected function getPackageProviders($app)
    {
        return [
            EnumerationServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.default', 'sqlite');
        $app['config']->set('database.connections.sqlite', [
            'driver' => 'sqlite',
            'database' => ':memory:',
            'prefix' => '',
        ]);
    }

    /**
     * Set up the database.
     *
     * @param \Illuminate\Foundation\Application $app
     */
    protected function setUpDatabase($app)
    {
        include_once __DIR__ . '/create_test_tables.php';
        (new \CreateTestTables())->up();
    }
}
